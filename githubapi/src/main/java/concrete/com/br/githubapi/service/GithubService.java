package concrete.com.br.githubapi.service;

import java.util.*;

import concrete.com.br.githubapi.model.pull_request.*;
import concrete.com.br.githubapi.model.repository.*;
import retrofit2.*;
import retrofit2.http.*;

/**
 * Created by rafael on 06/08/17.
 */

    public interface GithubService {

        @GET("/search/repositories?q=language:Java&sort=stars")
        Call<Repository> getRepository(
                @Query("page") int page
        );

        @GET("/repos/{creator}/{repository}/pulls")
        Call<ArrayList<PullRequest>> getPullRequest(
                @Path("creator") String creator,
                @Path("repository") String repository
        );



    }

