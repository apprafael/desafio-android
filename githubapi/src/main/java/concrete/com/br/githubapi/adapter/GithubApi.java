package concrete.com.br.githubapi.adapter;

import concrete.com.br.githubapi.service.*;
import retrofit2.*;
import retrofit2.converter.gson.*;

/**
 * Created by rafael on 06/08/17.
 */


public class GithubApi {

    private static GithubService githubservice;


    public static GithubService create()
    {
        if(githubservice == null)
        githubservice =  new Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(GithubService.class);

        return githubservice;
    }
}