package concrete.com.br.github.interactor.pullrequest;

/**
 * Created by rafael on 07/08/17.
 */

public interface IinteractorPullRequest {

        public void fechtData(String creator, String repository);
}
