package concrete.com.br.github;

import android.app.Application;

import concrete.com.br.github.DI.ApplicationComponent;
import concrete.com.br.github.DI.ApplicationModule;
import concrete.com.br.github.DI.DaggerApplicationComponent;


/**
 * Created by stormsec on 15/08/17.
 */

public class DesafioApplication extends Application {

    private ApplicationComponent applicationComponent;

    public ApplicationComponent getApplicationComponent()
    {
        return applicationComponent;
    }

    protected ApplicationComponent initDagger(DesafioApplication application) {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(application))
                .build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = initDagger(this);
    }
}
