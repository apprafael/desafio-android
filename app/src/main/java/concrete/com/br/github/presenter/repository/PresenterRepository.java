package concrete.com.br.github.presenter.repository;

import concrete.com.br.github.interactor.repository.*;
import concrete.com.br.github.view.activitys.repository.*;
import concrete.com.br.githubapi.model.repository.*;

/**
 * Created by rafael on 06/08/17.
 */
public class PresenterRepository implements IpresenterRepository.PresenterView, IpresenterRepository.PresenterInteracor{

    IviewRepository view;
    IinteractorRepository interactorRepository;

    public void setView(final IviewRepository view)
    {
        this.view = view;
    }

    @Override
    public void fetchData(int page) {

        interactorRepository = new InteractorRepository(this);
        interactorRepository.fechtData(page);
    }

    @Override
    public void success(Repository repository) {
        if(view!=null)
        view.Success(repository);
    }

    @Override
    public void failure(String msg) {
        if(view!=null)
        view.Failure(msg);
    }
}
