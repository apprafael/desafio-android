package concrete.com.br.github.DI;

import javax.inject.Singleton;
import concrete.com.br.github.presenter.pullrequest.PresenterPullRequest;
import concrete.com.br.github.presenter.repository.PresenterRepository;
import dagger.Module;
import dagger.Provides;

/**
 * Created by stormsec on 15/08/17.
 */
@Module
public class PresenterModule {

    @Provides
    @Singleton
    PresenterRepository providerPresenterRepository()
    {
        return new PresenterRepository();
    }

    @Provides
    @Singleton
    PresenterPullRequest providerPresenterPullRequest(){return new PresenterPullRequest();}
}
