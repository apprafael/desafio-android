package concrete.com.br.github.view.activitys.repository;

import android.support.annotation.LayoutRes;
import android.os.Bundle;
import android.support.v7.widget.*;
import javax.inject.Inject;
import butterknife.*;
import concrete.com.br.github.R;
import concrete.com.br.github.presenter.repository.*;
import concrete.com.br.github.view.EndlessRecyclerViewScrollListener;
import concrete.com.br.github.view.activitys.BaseActivity;
import concrete.com.br.github.view.adapters.repository.*;
import concrete.com.br.githubapi.model.repository.*;


public class MainActivity extends BaseActivity implements IviewRepository {

    @BindView(R.id.repositories_list)
    RecyclerView repositoriesRecyclerView;

    @Inject
    PresenterRepository presenterRepository;
    LinearLayoutManager layoutManager;
    repositoryAdapter adapter;
    private EndlessRecyclerViewScrollListener scrollListener;

    private int page = 1;
    int listCurrentSize;

    Repository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getApplicationComponent().inject(this);
        setContentView(R.layout.activity_main);
        presenterRepository.setView(this);
        presenterRepository.fetchData(page);

        layoutManager = new LinearLayoutManager(this);
        repositoriesRecyclerView.setLayoutManager(layoutManager);

        scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                listCurrentSize = adapter.getItemCount();
                presenterRepository.fetchData(++page);
            }
        };

        repositoriesRecyclerView.addOnScrollListener(scrollListener);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
    }

    @Override
    public void Success(Repository repository) {

        if(this.repository!=null)
            this.repository.getItems().addAll(repository.getItems());
        else
            this.repository = repository;

        if(adapter==null)
        {
            adapter = new repositoryAdapter(repository,MainActivity.this);
            repositoriesRecyclerView.setAdapter(adapter);
        }else
        {
            adapter.notifyItemRangeChanged(listCurrentSize,this.repository.getItems().size()-1);
        }

    }

    @Override
    public void Failure(String error) {

    }
}
