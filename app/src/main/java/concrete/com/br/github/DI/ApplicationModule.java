package concrete.com.br.github.DI;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import concrete.com.br.github.presenter.repository.PresenterRepository;
import dagger.Module;
import dagger.Provides;

/**
 * Created by stormsec on 14/08/17.
 */

@Module
public class ApplicationModule {

    private final Application mApplication;

    public ApplicationModule(Application app) {
        mApplication = app;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mApplication;
    }


}