package concrete.com.br.github.DI;

import javax.inject.Singleton;

import concrete.com.br.github.view.activitys.BaseActivity;
import concrete.com.br.github.view.activitys.pullrequest.PullRequestActivity;
import concrete.com.br.github.view.activitys.repository.MainActivity;
import dagger.Component;

/**
 * Created by stormsec on 15/08/17.
 */
@Singleton
@Component(modules = {ApplicationModule.class,PresenterModule.class})
public interface ApplicationComponent {

    void inject(MainActivity target);
    void inject(PullRequestActivity target);

}
