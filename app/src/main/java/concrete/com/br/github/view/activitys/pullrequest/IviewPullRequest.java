package concrete.com.br.github.view.activitys.pullrequest;

import java.util.*;

import concrete.com.br.githubapi.model.pull_request.*;
import concrete.com.br.githubapi.model.repository.*;

/**
 * Created by rafael on 06/08/17.
 */

public interface IviewPullRequest {

    public void Success(ArrayList<PullRequest> pullRequests);

    public void Failure(String error);
}
