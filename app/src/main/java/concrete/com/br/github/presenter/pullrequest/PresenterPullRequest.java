package concrete.com.br.github.presenter.pullrequest;

import java.util.*;

import concrete.com.br.github.interactor.pullrequest.*;
import concrete.com.br.github.view.activitys.pullrequest.*;
import concrete.com.br.githubapi.model.pull_request.*;

/**
 * Created by rafael on 06/08/17.
 */

public class PresenterPullRequest implements IpresenterPullRequest.PresenterView,IpresenterPullRequest.PresenterInteracor{

    IviewPullRequest view;
    IinteractorPullRequest iteractorPullRequest;

    public void setView(final IviewPullRequest view) {

        this.view = view;
    }

    @Override
    public void fetchData(String creator,String repository) {
        iteractorPullRequest = new InteractorPullRequest(this);
        iteractorPullRequest.fechtData(creator,repository);
    }

    @Override
    public void success(ArrayList<PullRequest> pullRequests) {

        if(view!=null)
        view.Success(pullRequests);
    }

    @Override
    public void failure(String msg) {

        if(view!=null)
        view.Failure(msg);
    }
}
