package concrete.com.br.github.view.activitys.repository;

import concrete.com.br.githubapi.model.repository.*;

/**
 * Created by rafael on 06/08/17.
 */

public interface IviewRepository {

    public void Success(Repository repository);

    public void Failure(String error);
}
