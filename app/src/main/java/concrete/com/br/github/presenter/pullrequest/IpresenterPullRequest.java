package concrete.com.br.github.presenter.pullrequest;

import java.util.*;
import concrete.com.br.githubapi.model.pull_request.*;

/**
 * Created by rafael on 06/08/17.
 */

public interface IpresenterPullRequest {

    public interface PresenterView
    {
        public void fetchData(String creator, String repository);
    }

    public interface PresenterInteracor
    {
        public void success(ArrayList<PullRequest> pullRequests);

        public void failure(String msg);
    }
}
