package concrete.com.br.github.presenter.repository;

import concrete.com.br.github.view.activitys.repository.IviewRepository;
import concrete.com.br.githubapi.model.repository.*;

/**
 * Created by rafael on 06/08/17.
 */

public interface IpresenterRepository {

    public interface PresenterView
    {
        public void fetchData(int page);
        public void setView(IviewRepository view);
    }

    public interface PresenterInteracor
    {
        public void success(Repository repository);

        public void failure(String msg);
    }
}
