package concrete.com.br.github.view.activitys.pullrequest;

import android.os.*;
import android.support.annotation.LayoutRes;
import android.support.v7.app.*;
import android.support.v7.widget.*;
import java.util.*;

import javax.inject.Inject;

import butterknife.*;
import concrete.com.br.github.DesafioApplication;
import concrete.com.br.github.R;
import concrete.com.br.github.presenter.pullrequest.*;
import concrete.com.br.github.view.activitys.BaseActivity;
import concrete.com.br.github.view.adapters.pullrequest.*;
import concrete.com.br.githubapi.model.pull_request.*;

/**
 * Created by rafael on 07/08/17.
 */

public class PullRequestActivity extends BaseActivity implements IviewPullRequest{
    @BindView(concrete.com.br.github.R.id.repositories_list)
    RecyclerView repositoriesRecyclerView;
    @Inject
    PresenterPullRequest presenterPullRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getApplicationComponent().inject(this);
        presenterPullRequest.setView(this);
        presenterPullRequest.fetchData(getIntent().getStringExtra("creator"),getIntent().getStringExtra("repository"));
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
    }

    @Override
    public void Success(ArrayList<PullRequest> pullRequests) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(PullRequestActivity.this);
        repositoriesRecyclerView.setLayoutManager(layoutManager);
        pullRequestAdapter adapter = new pullRequestAdapter(pullRequests,PullRequestActivity.this);
        repositoriesRecyclerView.setAdapter(adapter);
    }

    @Override
    public void Failure(String error) {

    }
}

