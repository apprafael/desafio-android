package concrete.com.br.github.view.activitys;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.ButterKnife;
import concrete.com.br.github.DI.ApplicationComponent;
import concrete.com.br.github.DesafioApplication;

/**
 * Created by stormsec on 16/08/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    protected ApplicationComponent getApplicationComponent()
    {
        return ((DesafioApplication)getApplication()).getApplicationComponent();
    }

}
