package concrete.com.br.github.interactor.pullrequest;

import java.util.*;

import concrete.com.br.github.presenter.pullrequest.*;
import concrete.com.br.githubapi.adapter.*;
import concrete.com.br.githubapi.model.pull_request.*;
import retrofit2.*;

/**
 * Created by rafael on 07/08/17.
 */

public class InteractorPullRequest implements IinteractorPullRequest {

    IpresenterPullRequest.PresenterInteracor presenterInteracor;

    public  InteractorPullRequest(IpresenterPullRequest.PresenterInteracor presenterInteracor) {
        this.presenterInteracor = presenterInteracor;
    }

    @Override
    public void fechtData(String creator,String repository) {

        GithubApi.create().getPullRequest(creator,repository).enqueue(new Callback<ArrayList<PullRequest>>() {
            @Override
            public void onResponse(Call<ArrayList<PullRequest>> call, Response<ArrayList<PullRequest>> response) {
                presenterInteracor.success(response.body());
            }

            @Override
            public void onFailure(Call<ArrayList<PullRequest>> call, Throwable t) {
                presenterInteracor.failure(t.getLocalizedMessage());
            }
        });
    }
}
