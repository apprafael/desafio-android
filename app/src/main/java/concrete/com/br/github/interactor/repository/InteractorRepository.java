package concrete.com.br.github.interactor.repository;

import concrete.com.br.github.presenter.repository.*;
import concrete.com.br.githubapi.adapter.*;
import concrete.com.br.githubapi.model.repository.*;
import retrofit2.*;

/**
 * Created by rafael on 07/08/17.
 */

public class InteractorRepository implements IinteractorRepository {

    IpresenterRepository.PresenterInteracor presenterInteracor;

    public InteractorRepository(IpresenterRepository.PresenterInteracor presenterInteracor) {
        this.presenterInteracor = presenterInteracor;
    }

    @Override
    public void fechtData(int page) {

        GithubApi.create().getRepository(page).enqueue(new Callback<Repository>() {
            @Override
            public void onResponse(Call<Repository> call, Response<Repository> response) {
                presenterInteracor.success(response.body());
            }

            @Override
            public void onFailure(Call<Repository> call, Throwable t) {
                presenterInteracor.failure(t.getLocalizedMessage());
            }
        });
    }
}
